const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = [
	new HtmlWebpackPlugin({
		template: './src/index.pug',
		filename: './index.html'
	})
]