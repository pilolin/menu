import { hideScroll, showScroll } from './manageScrollDocument';
import './../scss/styles.scss';

class Menu {
	constructor() {
		this.button = document.getElementById('menu-toggle');
		this.menu = document.getElementById('menu');
		this.menuNav = menu.querySelector('.menu__nav');
		this.menuSectionsList = menu.querySelector('.menu__sections');
		this.menuBackground = menu.querySelector('.menu__background');

		this.init();
	}

	init() {
		this.button.addEventListener('click', () => { this.toggle(); });
	}

	open() {
		document.body.classList.add('menu-opened');
		this.menu.setAttribute('aria-expanded', true);

		this.menuBackground.classList.add('animate', 'fadeIn', 'fast');

		this.menuNav.classList.add('animate', 'fadeIn');

		this.menuSectionsList.classList.add('animate', 'fadeInUp', 'fast');

		hideScroll();
	}

	close() {
		this.menuBackground.classList.remove('fadeIn');
		this.menuBackground.classList.add('fadeOut');

		this.menuNav.classList.remove('fadeIn');
		this.menuNav.classList.add('fadeOut');

		this.menuSectionsList.classList.remove('fadeInUp');
		this.menuSectionsList.classList.add('fadeOutUp');

		setTimeout(() => {
			document.body.classList.remove('menu-opened');
			this.menu.setAttribute('aria-expanded', false);
			this.menuBackground.classList.remove('animate', 'fadeOut', 'fast');
			this.menuNav.classList.remove('animate', 'fadeOut');
			this.menuSectionsList.classList.remove('animate', 'fadeOutUp', 'fast');
		}, 300)

		showScroll();
	}

	toggle() {
		if( document.body.classList.contains('menu-opened') && this.menu.getAttribute('aria-expanded') === 'true' ) {
			this.close();
		} else {
			this.open();
		}
	}
}

new Menu();